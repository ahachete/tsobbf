#!/bin/sh

eksctl --region us-west-2 create cluster \
	--name tsomssql \
	--node-type c7g.xlarge \
	--node-volume-size 100 \
	--nodes 2 \
	--zones us-west-2a,us-west-2b,us-west-2c \
	--version 1.19
