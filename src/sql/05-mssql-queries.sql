select
	[trading].time_bucket('1 week', s_date) as weekly,
	max(t_high)
from		[trading].stock
where		nasdaq = 'AAPL'
group by	weekly
order by	weekly asc
;
