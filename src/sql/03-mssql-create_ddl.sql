create table [trading].symbol (
	nasdaq		text		primary key
,	security 	text
);

create table [trading].stock (
	s_date		date
,	nasdaq		text
,	t_open		real
,	t_high		real
,	t_low		real
,	t_close		real
,	volume		real
,	constraint stock_pk primary key (nasdaq, s_date)
,	constraint stock_nasdaq_fk foreign key (nasdaq) references [trading].symbol (nasdaq)
);
