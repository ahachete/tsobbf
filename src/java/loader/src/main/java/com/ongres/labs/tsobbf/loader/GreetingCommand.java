package com.ongres.labs.tsobbf.loader;

import org.jboss.logging.Logger;
import picocli.CommandLine;
import picocli.CommandLine.Command;


@Command
public class GreetingCommand implements Runnable {
    private static final Logger LOG = Logger.getLogger(GreetingCommand.class);

    @CommandLine.Option(names = {"-h", "--host"})
    String host;

    @CommandLine.Option(names = {"-p", "--port"}, defaultValue = "1433")
    int port;

    @CommandLine.Option(names = {"-u", "--user"})
    String user;

    @CommandLine.Option(names = {"-d", "--database"}, defaultValue = "master")
    String database;

    @CommandLine.Option(names = {"-w", "--password"})
    String password;

    @CommandLine.Option(names = {"-y", "--last-year"}, defaultValue = "1999")
    int lastYear;

    @Override
    public void run() {
        var loader = new Loader(host, port, user, database, password, lastYear);
        try {
            loader.loadData();
        } catch (LoaderException e) {
            LOG.fatal(e.getMessage());
        }
    }
}
