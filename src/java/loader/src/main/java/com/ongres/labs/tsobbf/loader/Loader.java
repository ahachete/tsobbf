package com.ongres.labs.tsobbf.loader;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.jboss.logging.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.time.LocalDate;
import java.util.zip.GZIPInputStream;


public class Loader {
    private static final Logger LOG = Logger.getLogger(Loader.class);
    private static final int BATCH_SIZE = 1_000;
    private static final int N_THREADS = 4;
    private static final int FIRST_YEAR = 1980;
    private static final String SQL_PS_INSERT_SYMBOL = "insert into [trading].symbol (security, nasdaq) values (?, ?)";
    private static final String SQL_PS_INSERT_STOCK = """
            insert into [trading].stock (s_date, nasdaq, t_open, t_high, t_low, t_close, volume)
            values (?, ?, ?, ?, ?, ?, ?)
    """;

    private final String host;
    private final int port;
    private final String user;
    private final String database;
    private final String password;
    private final int lastYear;

    public Loader(String host, int port, String user, String database, String password, int lastYear) {
        this.host = host;
        this.port = port;
        this.user = user;
        this.database = database;
        this.password = password;
        this.lastYear = lastYear;
    }

    private String connectionUrl() {
        return "jdbc:sqlserver://" + host + ";"
                + "database=" + database + ";"
                + "encrypt=false";
    }

    private interface SqlExceptionBiConsumer<A,B> {
        void accept(A a, B b) throws SQLException;
    }

    public void loadData() throws LoaderException {
        try(Connection connection = DriverManager.getConnection(connectionUrl(), user, password)) {
            connection.setAutoCommit(true);

            loadCsvGzSymbols(connection);
            connection.commit();

            for(int i = FIRST_YEAR; i <= lastYear; i++) {
                loadCsvGzStocks(connection, i);
                connection.commit();
            }
        } catch (SQLException e) {
            LOG.error("Error connecting to the database: " + e.getLocalizedMessage());
        }
    }

    private void loadCsvGzResource(
            String fileName, Connection connection, String sql,
            SqlExceptionBiConsumer<PreparedStatement,CSVRecord> consumer
    ) {
        var classLoader = Thread.currentThread().getContextClassLoader();

        try(var csvGz = classLoader.getResourceAsStream(fileName)) {
            try(var csv = new BufferedReader(new InputStreamReader(new GZIPInputStream(csvGz)))) {
                var csvParser = CSVFormat.RFC4180.parse(csv);

                try (var ps = connection.prepareStatement(sql)) {
                    int i = 0;
                    for (CSVRecord csvRecord : csvParser.getRecords()) {
                        consumer.accept(ps, csvRecord);
                        ps.addBatch();
                        if (i++ % BATCH_SIZE == 0) {
                            ps.executeBatch();
                        }
                    }
                    ps.executeBatch();
                }
            }
        } catch (SQLException | IOException e) {
            LOG.error("Error loading csv.gz resource: " + e.getLocalizedMessage(), e);
        }
    }

    private void loadCsvGzSymbols(Connection connection) {
        loadCsvGzResource(
                "symbols.csv.gz",
                connection,
                SQL_PS_INSERT_SYMBOL,
                (preparedStatement, csvRecord) -> {
                    preparedStatement.setString(1, csvRecord.get(0));
                    preparedStatement.setString(2, csvRecord.get(1));
                }
        );
    }

    private void loadCsvGzStocks(Connection connection, int year) {
        loadCsvGzResource(
                year + ".csv.gz",
                connection,
                SQL_PS_INSERT_STOCK,
                (ps, csvRecord) -> {
                    ps.setDate(1, Date.valueOf(LocalDate.parse(csvRecord.get(0))));
                    ps.setString(2, csvRecord.get(1));
                    ps.setDouble(3, getDoubleFromCsvField(csvRecord, 2));
                    ps.setDouble(4, getDoubleFromCsvField(csvRecord, 3));
                    ps.setDouble(5, getDoubleFromCsvField(csvRecord, 4));
                    ps.setDouble(6, getDoubleFromCsvField(csvRecord, 5));
                    ps.setDouble(7, getDoubleFromCsvField(csvRecord, 6));
                }
        );
    }

    private double getDoubleFromCsvField(CSVRecord csvRecord, int pos) {
        var field = csvRecord.get(pos);

        return field.isEmpty() ? 0 : Double.parseDouble(field);
    }
}
